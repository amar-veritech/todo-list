// Create a "close" button and append it to each list item
var myNodelist = document.getElementsByTagName('LI')
var i
for (i = 0; i < myNodelist.length; i++) {
  var span = document.createElement('SPAN')
  var txt = document.createTextNode('\u00D7')
  span.className = 'close'
  span.appendChild(txt)
  myNodelist[i].appendChild(span)
}

// Click on a close button to hide the current list item
var close = document.getElementsByClassName('close')
var i
for (i = 0; i < close.length; i++) {
  close[i].onclick = function () {
    var div = this.parentElement
    div.style.display = 'none'
  }
}

// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul')
list.addEventListener(
  'click',
  function (ev) {
    if (ev.target.tagName === 'LI' || ev.target.className === 'clicking') {
      const el = ev.target.closest('LI')
      el.classList.toggle('checked')
    }
  },
  false,
)

// Create a new list item when clicking on the "Add" button
function newElement() {
  var li = document.createElement('li')
  var inputValue = document.getElementById('myInput').value
  var selectValue = document.getElementById('mySelect').value
  li.innerHTML = `<span class='clicking'><strong class='clicking'>Category: </strong>${selectValue} &nbsp; &nbsp; <strong class='clicking'>Task: </strong>${inputValue} </span>`
  if (inputValue === '') {
    alert('You must write something!')
  } else if (selectValue === '') {
    alert('Please select the category')
  } else {
    document.getElementById('myUL').appendChild(li)
  }
  document.getElementById('myInput').value = ''
  document.getElementById('mySelect').value = ''

  var span = document.createElement('SPAN')
  var txt = document.createTextNode('\u00D7')
  span.className = 'close'
  span.appendChild(txt)
  li.appendChild(span)

  for (i = 0; i < close.length; i++) {
    close[i].onclick = function () {
      var div = this.parentElement
      div.style.display = 'none'
    }
  }
}
